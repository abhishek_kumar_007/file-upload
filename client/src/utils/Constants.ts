export default {
    uploadEndpoint: "http://localhost:3000/upload",
    defaultDelimiter: ",",
    defaultLines: 2
}
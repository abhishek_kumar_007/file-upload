import React from 'react';
import DataTableUpload from "./components/DataTable/DataTableUpload";
import './App.css';

function App() {
    return (
        <div className="App">
            <header className="App-header">
                File Uploader
            </header>
            <DataTableUpload/>
        </div>
    );
}

export default App;

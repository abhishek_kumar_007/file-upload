import React, {FC} from "react"

interface Props {
    percentage: number
}

const Progress: FC<Props> = ({percentage}) => {
    return (
        <div className="progress">
            <div
                className="progress-bar progress-bar-striped bg-success"
                role="progressbar"
                style={{width: `${percentage}%`}}
            >
                {percentage}%
            </div>
        </div>
    );
};

export default Progress

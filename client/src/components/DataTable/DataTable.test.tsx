import React from "react";
import Enzyme from "enzyme";
import ReactDOM from "react-dom";
import Adapter from "enzyme-adapter-react-16";
import DataTable from "./DataTable";

Enzyme.configure({adapter: new Adapter()});

describe("DataTable Component", () => {
    it("Render without crash", () => {
        const div = document.createElement("div");
        ReactDOM.render(
            <DataTable
                fileData={{
                    fileName: "dummy.txt",
                    data: [["A", "B", "C", "D", "E"], ["F", "G", "H", "I", "J"]]
                }}
            />,
            div
        );
        ReactDOM.unmountComponentAtNode(div);
    });
});

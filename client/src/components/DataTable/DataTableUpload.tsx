import React, {useCallback, useState, ChangeEvent} from "react";
import DataTable from "./DataTable";
import FileUpload from "../FileUpload/FileUpload";
import {FileUploadData} from "../../communicators/FileUpload";
import styles from "./dataTableUpload.module.css";
import Constants from "../../utils/Constants";

const DataTableUpload = () => {

    const [fileData, setFileData] = useState<FileUploadData | null>(null);

    const [delimiter, setDelimiter] = useState<string>(Constants.defaultDelimiter);
    const [rows, setRos] = useState<number | "">(Constants.defaultLines);

    const memoOnDelimiterChange = useCallback(
        (event: ChangeEvent<HTMLInputElement>) => {
            setDelimiter(event.target.value);
        }, [setDelimiter]);

    const memoOnRowsChange = useCallback(
        (event: ChangeEvent<HTMLInputElement>) => {
            if (event.target.value === "") {
                setRos("");
            } else {
                setRos(Number(event.target.value));
            }
        }, [setRos]);

    const memeOnFileUploaded = useCallback(
        (data: FileUploadData) => {
            setFileData(data);
        }, [setFileData]);

    const memoOnResetClick = useCallback(() => {
        setFileData(null);
    }, [setFileData]);

    return (
        <section className={styles.container}>
            <div className={styles.filerContainer}>
                <div>
                    Delimiter: <input value={delimiter} onChange={memoOnDelimiterChange} disabled={!!fileData}/>
                </div>
                <div>
                    Lines: <input value={rows} onChange={memoOnRowsChange} type="number" disabled={!!fileData}/>
                </div>
            </div>
            <div>
                {
                    fileData ? (
                        <button className="btn btn-secondary" onClick={memoOnResetClick}>Reset</button>
                    ) : null
                }
            </div>
            <div className={styles.dataContainer}>
                {
                    !fileData ? (
                        <FileUpload
                            delimiter={delimiter}
                            rowsLimit={Number(rows)}
                            onUploaded={memeOnFileUploaded}
                        />
                    ) : (
                        <DataTable fileData={fileData}/>
                    )
                }
            </div>
        </section>
    );
}

export default DataTableUpload;
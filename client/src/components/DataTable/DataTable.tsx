import React, {FC} from "react";
import {FileUploadData} from "../../communicators/FileUpload";

interface Props {
    fileData: FileUploadData
}

const DataTable: FC<Props> = ({fileData}) => {

    const tableDom = fileData.data.map((row, index) => {
        return (
            <tr key={index}>
                {
                    row.map((cellData, index) => {
                        return (<td key={index}>{cellData}</td>);
                    })
                }
            </tr>
        )
    });
    return (
        <table className="table table-striped table-bordered table-hover table-dark">
            <tbody>
            {tableDom}
            </tbody>
        </table>
    );
};

export default DataTable;
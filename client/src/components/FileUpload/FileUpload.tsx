import React, {ChangeEvent, FC, Fragment, useCallback, useState} from "react";
import Message from "../shared/Message";
import Progress from "../shared/Progress";
import FileUploadCommunicator, {FileUploadData} from "../../communicators/FileUpload";

import styles from "./fileUploaded.module.css";

interface Props {
    rowsLimit: number;
    delimiter: string;
    onUploaded: (data: FileUploadData) => void;
}

const FileUpload: FC<Props> = ({onUploaded, rowsLimit, delimiter}) => {
    const [file, setFile] = useState<File | Blob>();
    const [message, setMessage] = useState<string>("");
    const [uploadPercentage, setUploadPercentage] = useState<number>(0);

    const memoOnFileChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.files) {
            setFile(event.target.files[0]);
        }
    }, [setFile]);

    const memoOnSubmit = useCallback(async e => {
        e.preventDefault();

        try {
            const response = await FileUploadCommunicator.uploadFile(
                (file as Blob),
                rowsLimit,
                delimiter,
                (percentage) => {
                    setUploadPercentage(percentage);
                }
            );
            if (response) {
                onUploaded(response.data);
            } else {
                setTimeout(() => setUploadPercentage(0), 0);
                setMessage("There was a problem with the server");
            }

        } catch (err) {
            setTimeout(() => setUploadPercentage(0), 0);
            setMessage("There was a problem with the server");
        }
    }, [setMessage, onUploaded, file, setUploadPercentage, rowsLimit, delimiter]);

    return (
        <Fragment>
            {message ? <Message msg={message}/> : null}
            <div className={styles.formContainer}>
                <form onSubmit={memoOnSubmit}>
                    <div className="mb-4">
                        <input type="file" onChange={memoOnFileChange}/>
                    </div>

                    <div className="mb-4">
                        <Progress percentage={uploadPercentage}/>

                        <input
                            type="submit"
                            value="Upload"
                            className="btn btn-primary btn-block mt-4"
                            disabled={!file || !rowsLimit || !delimiter}
                        />
                    </div>
                </form>
            </div>
        </Fragment>
    );
};

export default FileUpload;

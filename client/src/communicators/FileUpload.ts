import axios, {AxiosResponse} from "axios";
import Constants from "../utils/Constants";

export interface FileUploadData {
    fileName: string,
    data: Array<Array<string>>
}

export type FileUploadResponse = AxiosResponse<FileUploadData>;


class FileUpload {

    static uploadFile = async (
        file: File | Blob,
        limit: number,
        delimiter: string,
        onUploadProgress: (percentage: number) => void
    ): Promise<FileUploadResponse> => {

        const formData = new FormData();
        formData.append("file", file);
        formData.append("limit", String(limit));
        formData.append("delimiter", delimiter);

        try {
            return axios.post(Constants.uploadEndpoint, formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                },
                onUploadProgress: progressEvent => {
                    onUploadProgress(parseInt(String(Math.round((progressEvent.loaded * 100) / progressEvent.total))));
                }
            });

        } catch (err) {
            throw Error(err);
        }
    }
}

export default FileUpload;
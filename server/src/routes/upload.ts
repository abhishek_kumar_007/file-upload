import * as fs from "fs";
import {IncomingForm} from "formidable";
import {Request, Response} from "express";

const allowedFilesTypes = ["text/plain"];

export function upload(req: Request, res: Response) {

    try {
        const form = new IncomingForm();
        form.multiples = true;

        form.on("field", function (field, value) {
            console.log("field: ", field);
            console.log("value: ", value);
        });

        form.parse(req, (err, fields, files) => {
            // @ts-ignore
            const filePath: string = files.file.path;

            if (filePath && allowedFilesTypes.indexOf(files.file.type) >= -1) {
                fs.readFile(filePath, {encoding: "utf-8"}, function (err, data) {
                    if (!err) {
                        console.log("received data: " + data);
                        const response = [];
                        if (data && fields.delimiter && fields.limit) {
                            let nRows = Number(fields.limit);

                            const delimiter = String(fields.delimiter);

                            const rows = data.split("\n");

                            if (rows.length < nRows) {
                                nRows = rows.length;
                            }
                            for (let i = 0; i < nRows; i++) {
                                response.push(rows[i].split(delimiter));
                            }

                            return res.json({data: response});
                        }
                    }

                    return res.status(400).send({
                        message: "Something went wrong while uploading"
                    });
                });
            } else {
                return res.status(400).send({
                    message: "Something went wrong while uploading"
                });
            }
        });
    } catch (e) {
        console.log(e);
    }
}

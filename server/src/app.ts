import cors from "cors";
import express from "express";
import bodyParser from "body-parser";
import {upload} from "./routes/upload";

const port = 3000;

const app = express();

const corsOptions = {
    origin: "*"
};

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());
app.use(cors(corsOptions));

app.post("/upload", upload);

app.listen(port, () => {
    console.log("Server started!");
});

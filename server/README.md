# File upload api

#Sample Request to upload:

fetch("http://localhost:3000/upload", {
  "headers": {
    "accept": "application/json, text/plain, */*",
    "accept-language": "en-US,en;q=0.9",
    "content-type": "multipart/form-data; boundary=----WebKitFormBoundarypNHvlbaCBVngzrW2",
  },
  "referrer": "http://localhost:3001/",
  "referrerPolicy": "no-referrer-when-downgrade",
  "body": "------WebKitFormBoundarypNHvlbaCBVngzrW2\r\nContent-Disposition: form-data; name=\"file\"; filename=\"sample.txt\"\r\nContent-Type: text/plain\r\n\r\n\r\n------WebKitFormBoundarypNHvlbaCBVngzrW2\r\nContent-Disposition: form-data; name=\"limit\"\r\n\r\n2\r\n------WebKitFormBoundarypNHvlbaCBVngzrW2\r\nContent-Disposition: form-data; name=\"delimiter\"\r\n\r\n,\r\n------WebKitFormBoundarypNHvlbaCBVngzrW2--\r\n",
  "method": "POST",
  "mode": "cors",
  "credentials": "omit"
});


Installation
----

#### Prerequisite

* You need to have [Node.js](https://nodejs.org/en/download/) (> 8) installed.
* [Yarn](https://yarnpkg.com/lang/en/docs/install/) for dependency management.
Usage
-----

#### How to run? ###
```shell script
Steps:
$ yarn install # if not done
$ yarn start # starts development serverless app locally
```

This will start a server on port 3000.
